﻿using MVCProject4.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

public interface IRepository : IDisposable
    {
        List<Address> GetAddressList();
        Address GetAddress(int id);
        void Create(Address address);
        void Update(Address address);
        void Delete(int id);
        void Save();
    }
