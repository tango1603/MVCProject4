﻿using MVCProject4.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVCProject4.Controllers
{
   class AddressRepository : IRepository
    {
        private AddressContext db;
        public AddressRepository()
        {
            this.db = new AddressContext();
        }
        public List<Address> GetAddressList()
        {
            return db.Addreses.ToList();
        }
        public Address GetComputer(int id)
        {
            return db.Addreses.Find(id);
        }

        public void Create(Address a)
        {
            db.Addreses.Add(a);
        }

        public void Update(Address a)
        {
            db.Entry(a).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Address a = db.Addreses.Find(id);
            if (a != null)
                db.Addreses.Remove(a);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Address GetAddress(int id)
        {
            throw new NotImplementedException();
        }
    }
}