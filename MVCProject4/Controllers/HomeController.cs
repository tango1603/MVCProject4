﻿using MVCProject4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCProject4.Controllers
{
    public class HomeController : Controller
    {
        //для теста данных
        IRepository repo;

        public HomeController()
        {
            repo = new AddressRepository();
        }

        public HomeController(IRepository ir)
        {
            repo = ir;
        }

        public ActionResult Index()
        {
            var model = repo.GetAddressList();
            if (model.Count > 0)
                ViewBag.Message = String.Format("В базе данных {0} объект", model.Count);
            return View(model);
        }
        protected override void Dispose(bool disposing)
        {
            repo.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult getData()
        {
            using (AddressContext dc = new AddressContext())
            {
                return Json(dc.Addreses.ToList());
            }
        }



    }
}