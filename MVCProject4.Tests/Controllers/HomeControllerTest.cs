﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVCProject4;
using MVCProject4.Controllers;
using MVCProject4.Models;
using Moq;

namespace MVCProject4.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            var mock = new Mock<IRepository>();
            //mock.Setup(a => a.GetAddressList()).Returns(new List<Address>());
            mock.Setup(a => a.GetAddressList()).Returns(new List<Address>() { new Address() });
            HomeController controller = new HomeController(mock.Object);
            string expected = "В базе данных 1 объект";

            // Act
            ViewResult result = controller.Index() as ViewResult;
            string actual = result.ViewBag.Message as string;

            // Assert
            Assert.IsNotNull(result.Model);
            Assert.AreEqual(expected, actual);
        }
    }
}
